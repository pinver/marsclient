

export function connectTo(url: string, client: any)
{
    //console.log('mars - trying to connect to server');
    client.$store.commit('setConnectionStatus', 'connecting');
    try {
        client.socketTo = new WebSocket(url + '_c2s');
        window.onbeforeunload = function() {
            console.log('mars - closing websocket on refresh');
            client.socketTo.onclose = function () {}; // disable the onclose handler first
            client.socketTo.close();
            client.$store.commit('setConnectionStatus', 'waitingForReconnection');
        };
        client.socketTo.onmessage = function (msg: any) {
            console.log('mars - mars.sockets.connectTo.onmessage(', msg, ')');
            client.$store.commit('setConnectionStatus', 'handlingMessages');
            client.onMessageTo(msg);
            client.$store.commit('setConnectionStatus', 'waitingForMessages');

        };
        client.socketTo.onopen = function() {
            client.$store.commit('setConnectionStatus', 'waitingForMessages');
            client.beginComunication(); 
        };
        client.socketTo.onclose = function() {
            console.log('C --- S - socket on close, scheduling reconnect...');
            if(client.pingTimer !== 0){ clearInterval(client.pingTimer); }
            scheduleReconnectTo(url, client);
        };
    }
    catch(e) {
        console.log('mars - connection failed: %s', e);
        scheduleReconnectTo(url, client);
    }
};

function scheduleReconnectTo(url: string, client: any)
{
    //console.log('mars - scheduling a reconnect...');
    client.$store.commit('setConnectionStatus', 'waitingForReconnection');
    setTimeout( function() {
        client.socket = null;
        connectTo(url, client);
    }, 5000);
}

export function connectFrom(url: string, client: any)
{
    //console.log('mars - trying to connect to server');
    client.$store.commit('setConnectionStatus', 'connecting');
    try {
        client.socketFrom = new WebSocket(url + '_s2c');
        window.onbeforeunload = function() {
            console.log('mars - closing websocket on refresh');
            client.socketFrom.onclose = function () {}; // disable the onclose handler first
            client.socketFrom.close();
            client.$store.commit('setConnectionStatus', 'waitingForReconnection');
        };
        // the onmessage for the socket will be set by the 'MsgHandlerFrom'...
        client.socketFrom.onopen = function() {
            // ... inform the service to client channed about the client id, so that the server can bind the push channel to the
            //     correct client instance
            console.log('C --> S | sending the client id for the server push channel:', client.clientid);
            client.socketFrom.send(client.clientid);
        };
        client.socketFrom.onclose = function() { scheduleReconnectFrom(url, client); };
        client.$store.commit('setConnectionStatus', 'waitingForMessages');
    }
    catch(e) {
        //console.log('mars - connection failed: %s', e);
        scheduleReconnectTo(url, client);
    }
};

function scheduleReconnectFrom(url: string, client: any)
{
    //console.log('mars - scheduling a reconnect...');
    client.$store.commit('setConnectionStatus', 'waitingForReconnection');
    setTimeout( function() {
        client.socket = null;
        //connectFrom(url, client);
    }, 5000);
}
