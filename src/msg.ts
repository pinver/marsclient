
declare function require(moduleName: string): any;
var msgpack: any = require('msgpack-lite');
import { SHA2_256 } from './sha2';

import { MarsClient } from './mars';

export
enum RequestState 
{
    executed,

    // ... client side bugs or tampering of the request
    rejectedAsDecodingFailed, /// the deconding of the data is failed
    rejectedAsWrongParameter, /// the value of one of the request parameters is wrong.

    rejectedAsNotAuthorised,  /// the client is not authorised for the request (ex, subscription before of authentication)
    rejectedAsForeignKeyViolation, /// update or delete on table violates foreign key constraint on another table (ERR 23503)

    rejectedAsPGSqlError,     /// PostgreSQL unhandled error
    internalServerError,
}

export enum MsgType
{
    authenticationRequest        = 0, authenticationReply        = 1,
    authenticateRequest          = 2, authenticateReply          = 3,
    discardAuthenticationRequest = 4, discardAuthenticationReply = 5,

    syncOperationRequest = 20, syncOperationReply = 21,
    importValuesRequest  = 22, importValuesReply  = 23,
    insertValuesRequest  = 24, insertValuesReply  = 25,
    updateValuesRequest  = 26, updateValuesReply  = 27, // server side update of the row...
    deleteRecordRequest  = 28, deleteRecordReply  = 29,

    optUpdateReq = 50, optUpdateRep = 51, // request the server to perform an update and confirm an optimistic one
    subscribeReq = 52, subscribeRep = 52, // request to subscribe to a query
    pingReq      = 54,                    // request to keep alive the connection, right now without a reply
    pesUpdateReq = 56, pesUpdateRep = 57, // request the server to perform an update and confirm the result

    welcomeBroadcast = 100, goodbyBroadcast = 101,

    callServerMethodRequest = 150, callServerMethodReply = 151,

    disconnectRequest = 200,
    aborting          = 201,
};

export enum MsgTypeStoC {
    autologinReq     = 60, autologinRep     = 61,
    syncOperationReq = 62, syncOperationRep = 63,
    importRecordsReq = 64, importRecordsRep = 65,
    deleteRecordsReq = 66, deleteRecordsRep = 67,
    insertRecordsReq = 68, insertRecordsRep = 69,
    updateRecordsReq = 70, updateRecordsRep = 71,
    pingReq          = 72, pingRep          = 73,
}

var options = { codec: msgpack.createCodec({ useraw: true }) };

// ---- Server to Client Request Messages ---

interface AutologinReq { username: string, sqlCreateDatabase: string, sqlStatements: string[], jsStatements: string[] }

function decodeAutologinReq(data: Uint8Array): AutologinReq {
    let message: AutologinReq = msgpack.decode(data, options);
    message.username = message.username.toString();
    if( message.sqlCreateDatabase ) message.sqlCreateDatabase = message.sqlCreateDatabase.toString();
    if( message.sqlStatements ) for(let i=0; i <message.sqlStatements.length; ++i){
        message.sqlStatements[i] = message.sqlStatements[i].toString();
    }
    if( message.jsStatements ) for(let i=0; i <message.jsStatements.length; ++i){
        message.jsStatements[i] = message.jsStatements[i].toString();
    }
    return message;
}
// ---
interface DeleteRecordsReq { tableIndex: number, statementIndex: number, encodedRecords: Array<Object> }

function decodeDeleteRecordsReq(data: Uint8Array): DeleteRecordsReq {
    let message: DeleteRecordsReq = msgpack.decode(data, options);
    message.encodedRecords = msgpack.decode(message.encodedRecords); // ... array of records, as array of Json object
    return message;
}

function encodeDeleteRecordsRep(): any {
    return msgpack.encode();
}

// ---
interface InsertRecordsReq { tableIndex: number; statementIndex: number; encodedRecords: Array<Object> }

function decodeInsertRecordsReq(data: Uint8Array): InsertRecordsReq {
    const message = msgpack.decode(data, options); // ... unpack the 'encodedRecords' as ubyte[]
    message.encodedRecords = msgpack.decode(message.encodedRecords); // ... array of records, as array of Json object
    return message;
}
function encodeInsertRecordsRep(): any {
    return msgpack.encode();
}
// ---
export interface UpdateRecord { keys: any; record: any }
interface UpdateRecordsReq { tableIndex: number, encodedRecords: Array<UpdateRecord> }

function decodeUpdateRecordsReq(data: Uint8Array): UpdateRecordsReq {
    const message = msgpack.decode(data, options); // ... unpack the 'encodedRecords' as ubyte[]
    message.encodedRecords = msgpack.decode(message.encodedRecords); // ... array of structures, as array of Json object
    return message;
}
function encodeUpdateRecordsRep(): any {
    return msgpack.encode();
}
// ---
interface ImportRecordsReq { tableIndex: number; statementIndex: number; encodedRecords: Array<Object> }

function decodeImportRecordsReq(data: Uint8Array): ImportRecordsReq {
    const message = msgpack.decode(data, options); // ... unpack the 'encodedRecords' as ubyte[]
    message.encodedRecords = msgpack.decode(message.encodedRecords); // ... array of records, as array of Json object
    return message;
}
function encodeImportRecordsRep(): any {
    return msgpack.encode();
}

// ---
enum SyncOperation { starting, completed }
interface SyncOperationReq {
    operation: SyncOperation;
}
function decodeSyncOperationReq(data: Uint8Array): SyncOperationReq {
    return msgpack.decode(data, options);
}
function encodeSyncOperationRep(): any {
    return msgpack.encode();
}
//

// ---- Client to Server  -----
export interface OptUpdateReq { tableIndex: number, keys: Object, record: any, sync: any; originalRecord: any }
export interface OptUpdateRep { state: RequestState }

/// the original record is right now not transmitted in the encoded request. It's needed if we need to rollback.
export function encodeOptUpdateReq(req: OptUpdateReq): [MsgType, any] {
    return [
        MsgType.optUpdateReq,
        msgpack.encode({ 
            tableIndex: req.tableIndex,
            keys: msgpack.encode(req.keys, options),
            record: msgpack.encode(req.record, options),
        })
    ];
}
function decodeOptUpdateRep(data: Uint8Array): OptUpdateRep {
     return msgpack.decode(data, options);
}

// ----
export interface PesUpdateReq { tableIndex: number, keys: Object, record: any }
export interface PesUpdateRep { state: RequestState }

/// the original record is right now not transmitted in the encoded request. It's needed if we need to rollback.
export function encodePesUpdateReq(req: PesUpdateReq): [MsgType, any] {
    return [
        MsgType.pesUpdateReq,
        msgpack.encode({ 
            tableIndex: req.tableIndex,
            keys: msgpack.encode(req.keys, options),
            record: msgpack.encode(req.record, options),
        })
    ];
}
function decodePesUpdateRep(data: Uint8Array): PesUpdateRep {
     return msgpack.decode(data, options);
}


// ------
export function encodePingReq() : [MsgType, any] {
    return [
        MsgType.pingReq,
        msgpack.encode(0)
    ];
}

 // -------
 export interface SubscribeReq { select: string, parameters: Object, vm: any, name: string, columns: Array<string> }
 export interface SubscribeRep { state: RequestState, json: any }

 export function encodeSubscribeReq(req: SubscribeReq): [MsgType, any] {
    return [
        MsgType.subscribeReq,
        msgpack.encode({
            select: req.select,
            parameters: req.parameters,
        })
     ];
 }

 function decodeSubscribeRep(data: Uint8Array): SubscribeRep {
     let msg = msgpack.decode(data);
     msg.json = JSON.parse(msg.json);
     return msg;
 }

// --


interface AuthenticationRequest {
    username: string;
};



interface AuthenticateRequest { hash: string; }
function authenticateRequest(hash: string): AuthenticateRequest
{
    return { hash };
};

const authenticateReply = function(data: any) // ArrayBuffer data
{
    const message = msgpack.decode(data, options);
    console.log('decoded as authenticateReply:', message);
    if( message.sqlCreateDatabase ) message.sqlCreateDatabase = message.sqlCreateDatabase.toString();
    if( message.sqlStatements ) for(let i=0; i <message.sqlStatements.length; ++i){
        message.sqlStatements[i] = message.sqlStatements[i].toString();
    }
    if( message.jsStatements ) for(let i=0; i <message.jsStatements.length; ++i){
        message.jsStatements[i] = message.jsStatements[i].toString();
    }
    //console.log('decoded as authenticateReply:', message);
    return message;
};



const importValuesRequest = function(data: any)
{
    const message = msgpack.decode(data, options); // ... unpack the 'bytes' as ubyte[]
    message.bytes = msgpack.decode(message.bytes); // ... to string
    //console.log('decoded as importValueRequest:', message);
    return message;
}

const importValuesReply = function()
{
    return { inserted: 0 };
}

const insertValuesRequest = function(data: any)
{
    const message = msgpack.decode(data, options);
    message.bytes = msgpack.decode(message.bytes);
    //console.log('decoded as insertValueRequest:', message);
    return message;
}

export const frzInsertValuesRequest = function(statementIndex: Number, record: Object){
    let s = msgpack.encode(record, options); // ... string to ubyte
    const message = { statementIndex: statementIndex, bytes: msgpack.encode(record, options) };
    return message;
}

const insertValuesReply = function()
{
    return { inserted: 0 };
};

enum InsertError {
    assertCheck,
    inserted,
    duplicateKeyViolations,
    unknownError,
}
// travels from Server to Client
const mltInsertValueReply = function(data: any)
{
    const message = msgpack.decode(data, options);
    console.log("decoded as mltInsertValueReply (assertCheck:0, inserted:1, duplicateKeyViolation:2, unknown:3):", message);
    // puo essere nullo, se e' fallito lato server: refactory!
    if( message.bytes != null ) message.bytes = msgpack.decode(message.bytes);
    if( message.clientKeys != null ) message.clientKeys = msgpack.decode(message.clientKeys);
    return message;
}

const deleteRecordRequest = function(data: any)
{
    const message = msgpack.decode(data, options);
    message.bytes = msgpack.decode(message.bytes);
    console.log('decoded as deleteRecordRequest:', message);
    return message;
}
const deleteRecordReply = function()
{
    return { deleted: 0 };
};
// travels from client to server: index is the table index.
export const frzDeleteRecordRequest = function(statementIndex: Number, record: Object){
    const message = { statementIndex: statementIndex, bytes: msgpack.encode(record, options) };
    return message
}
export const mltDeleteRecordRequest = function(data: any) {
    data.decodedBytes = msgpack.decode(data.bytes);
    return data;
}

const mltDeleteRecordReply = function(data: any) {
    const message = msgpack.decode(data, options);
    if( message.serverRecord != null )  message.serverRecord = msgpack.decode(message.serverRecord);
    return message;
}

const updateValuesRequest = function(data: any)
{
    const message = msgpack.decode(data, options);
    message.bytes = msgpack.decode(message.bytes);
    //console.log('decoded as updateValueRequest:', message);
    return message;
}

const updateValuesReply = function()
{
    return { updated: 0 };
};

export const frzCallServerMethodRequest = function( methodName: string, parameters: Object ){
    const message = { method: methodName, parameters: JSON.stringify(parameters) };
    return message;
};
//interface CallServerMethodRequest { method: string, parameters: Object }
const callServerMethodReply = function(data: any)
{
    const message = msgpack.decode(data, options);
    message.returns = message.returns.toString();
    //console.log('decoded as callServerMethodReply:', message);
    return message;
}

export class MsgHandlerTo
{
    nextId: number;
    socket: any;
    messages: Map<any, any>;
    marsClient: MarsClient;

    constructor(socket: any, client: any) {
        this.nextId = 0;
        this.socket = socket;
        this.socket.binaryType = 'arraybuffer';
        this.messages = new Map();
        this.marsClient = client;

        this.socket.onmessage = (binaryMessage: any) => 
        {
            // ArrayBuffer binaryMessage.data
            //console.log('mars - mars.msg.MsgHandler.socket.onmessage( ... )');
            //console.log('mars - received binary message:%s ', new Int8Array(binaryMessage.data).toString());
            this.marsClient.$store.commit('setConnectionStatus', 'handlingMessages');
            
            // ... skip the first import of values as the database it's alredy in place...
            let skipImportValues = false;

            const dv = new DataView(binaryMessage.data);
            const msgId = dv.getInt32(0, true);
            const msgType = dv.getInt32(4, true);
            const msgEncoded = new Uint8Array(binaryMessage.data.slice(8));
            //console.log('mars - msg id %d of type %d', msgId, msgType);
            
            const msgRequest = this.messages.get(msgId);
            this.messages.delete(msgId);

            if( msgType == MsgType.authenticationReply ){
                const message = msgpack.decode(msgEncoded);
                console.log('C <-- S | authentication reply, status: %s (0:seedProvided, 1:invalidUsername, 2:alreadyAuthorised), seed:%s', message.status, message.seed);
                if( message.status === 0 ){
                    const hashPassword = SHA2_256(this.marsClient.$store.state.mars.credential.password).toUpperCase();
                    const hash = SHA2_256(message.seed + hashPassword).toUpperCase();
                    console.log('C --> S | authenticate request, password:%s hash:%s', this.marsClient.$store.state.mars.credential.password, hashPassword);
                    this.sendMessageRequest(MsgType.authenticateRequest, authenticateRequest(hash));
                }
                else if( message.status == 1 ){
                    this.marsClient.$store.commit('deauthorizeUser', { errors: 'Invalid Username! (empty, for example)' })
                }
            }
            else if( msgType == MsgType.authenticateReply ){
                const message = authenticateReply(msgEncoded);
                console.log('C <-- S | authenticate reply, status: %s (authorised:1, databaseOffline:2, wrongUsernameOrPassword:3, unknownError:4)', message.status);
                if( message.status === 1){
                    const wasAlreadyAuthorised = this.marsClient.$store.state.mars.credential.authorised === true;
                    this.marsClient.$store.commit('authorizeUser');
                    if( ! wasAlreadyAuthorised ){
                        // ... almost always it's a developer that was turning the server up/down frequently
                        //console.log('creating the database client side');
                        this.marsClient.$store.commit('setDataStatus', 'creatingDatabase');
                        this.marsClient.createDatabase(message.sqlCreateDatabase);
                        this.marsClient.prepareStatements(message.sqlStatements);
                        this.marsClient.prepareJsStatements(message.jsStatements);
                        this.marsClient.$store.commit('setDataStatus', 'waitingForLoading');
                    } else skipImportValues = true;
                }
                else if( message.status === 2){
                    this.marsClient.$store.commit('deauthorizeUser', { errors: 'The database is offline' });
                }
                else if( message.status === 3){
                    this.marsClient.$store.commit('deauthorizeUser', { errors: 'Wrong Username Or Password' });
                }
                else if( message.status === 4){
                    this.marsClient.$store.commit('deauthorizeUser', { errors: 'Unknown Database Error' });
                }
                else {
                    this.marsClient.$store.commit('deauthorizeUser', { errors: 'Unknown WebService Error' });
                }
                //console.log('mars - authentication reply processed');
            }
            else if( msgType == MsgType.discardAuthenticationReply ){
                console.log('mars - also the server discarded authorization for current user');
                this.marsClient.dropDatabase();
            }
            else if( msgType == MsgType.callServerMethodReply ){
                const message = callServerMethodReply(msgEncoded);
                console.log('C <-- S | call server method reply, method:%s, params:%s, reply:%s,', msgRequest.method, msgRequest.parameters, message.returns);
                this.marsClient.$store.commit('callServerMethodReply', [msgRequest.method, msgRequest.parameters, message.returns]);
            }
            else if( msgType == MsgType.insertValuesReply ){
                const message = mltInsertValueReply(msgEncoded);
                console.log('C <-- S | insert value reply, ', message);
                this.marsClient.onVueInsertRecordReply(msgRequest, message);
            }
            else if( msgType == MsgType.deleteRecordReply ){
                const message = mltDeleteRecordReply(msgEncoded);
                console.log('C <-- S | delete record reply, ', message);
                this.marsClient.onVueDeleteRecordReply(msgRequest, message);
            }
            else if( msgType == MsgType.optUpdateRep ){
                const message = decodeOptUpdateRep(msgEncoded);
                console.log('C <-- S | optimistic update record reply, ', message);
                this.marsClient.onVueUpdateReply(msgRequest, message);
            }
            else if( msgType == MsgType.pesUpdateRep ){
                const message = decodePesUpdateRep(msgEncoded);
                console.log('C <-- S | pessimistic update record reply, ', message);
                this.marsClient.onPesUpdateRep(msgRequest, message);
            }
            else if( msgType == MsgType.subscribeRep ){
                const message = decodeSubscribeRep(msgEncoded);
                console.log('C <-- S | subscribe reply, ', msgRequest, message);
                if(message.state == RequestState.executed){
                    msgRequest.vm[msgRequest.name] = buildTable(msgRequest.columns, message.json);
                    msgRequest.vm[msgRequest.name + '_mars_status'] = { loading: false };
                }
                else {
                    alert("something wrong server side:" + JSON.stringify(message));
                }
            }

            this.marsClient.$store.commit('setConnectionStatus', 'waitingForMessages');
            //console.log('mars - socket on message terminated, ready to receive an other message');
        };
    }

    sendReq(req: any, encoded: [MsgType, any]): void
    {
        let [ type, encodedReq ] = encoded;
        console.log(req, encoded);
        let prefix = new ArrayBuffer(8);
        let ui32 = new Uint32Array(prefix);
        ui32[0] = this.nextId; 
        ui32[1] = type;
        this.nextId += 1;

        this.messages.set(ui32[0], req);

        const tmp = new Uint8Array(prefix.byteLength + encodedReq.byteLength);
        tmp.set(new Uint8Array(prefix), 0);
        tmp.set(new Uint8Array(encodedReq), prefix.byteLength);
        const buffer = tmp.buffer;

        console.log('mars - send message request C->S %d of type %d', ui32[0], type);
        this.marsClient.$store.commit('setConnectionStatus', 'handlingMessages');
        this.socket.send(buffer);
        this.marsClient.$store.commit('setConnectionStatus', 'waitingForMessages');
        console.log('mars - sendMessageRequest C->S exiting');
    }

    // same as above, but encode here.
    sendMessageRequest(type: any, msg: Object)
    {
        let prefix = new ArrayBuffer(8);
        let ui32 = new Uint32Array(prefix);
        ui32[0] = this.nextId; 
        ui32[1] = type;
        this.nextId += 1;

        this.messages.set(ui32[0], msg);
        const encoded = msgpack.encode(msg);
        
        const tmp = new Uint8Array(prefix.byteLength + encoded.byteLength);
        tmp.set(new Uint8Array(prefix), 0);
        tmp.set(new Uint8Array(encoded), prefix.byteLength);
        const buffer = tmp.buffer;

        console.log('mars - send message request C->S %d of type %d', ui32[0], type);
        this.marsClient.$store.commit('setConnectionStatus', 'handlingMessages');
        this.socket.send(buffer);
        this.marsClient.$store.commit('setConnectionStatus', 'waitingForMessages');
        console.log('mars - sendMessageRequest C->S exiting');
    }
};

function buildTable(columns: any, table: any) {
    let builtTable = [];
    for(let row of table) {
        let builtRow: any = {};

        for(let colIndex = 0; colIndex < columns.length; ++colIndex) {
            const col = columns[colIndex];
            builtRow[col] = row[colIndex];
        }

        builtTable.push(builtRow);
    }
    return builtTable;
}

export class MsgHandlerFrom
{
    nextId: number;
    socket: any;
    messages: any;
    marsClient: MarsClient;

    constructor(socket: any, client: any) {
        this.nextId = 0;
        this.socket = socket;
        this.socket.binaryType = 'arraybuffer';
        this.messages = new Map();
        this.marsClient = client;

        this.socket.onmessage = (binaryMessage: any) => 
        {
            // ArrayBuffer binaryMessage.data
            //console.log('mars - mars.msg.MsgHandler.socket.onmessage( ... )');
            //console.log('mars - received binary message:%s ', new Int8Array(binaryMessage.data).toString());
            this.marsClient.$store.commit('setConnectionStatus', 'handlingMessages');
            
            // ... skip the first import of values as the database it's alredy in place...
            let skipImportValues = false;

            const dv = new DataView(binaryMessage.data);
            const msgId = dv.getInt32(0, true);
            const msgType = dv.getInt32(4, true);
            const msgEncoded = new Uint8Array(binaryMessage.data.slice(8));
            //console.log('mars - msg id %d of type %d', msgId, msgType);
            
            // ... broadcast of push message from server ...
            /*if( msgType == MsgType.welcomeBroadcast ){
                const message = msgpack.decode(msgEncoded);
                console.log('C <-- S | welcome broadcast! welcome %s', message.username);
            }
            else if( msgType == MsgType.goodbyBroadcast ){
                const message = msgpack.decode(msgEncoded);
                console.log('C <-- S | goodby broadcast! goodbye %s', message.username);
            }
            else*/ if( msgType == MsgTypeStoC.autologinReq ){
                console.log('C <-- S | autologin request');
                const message = decodeAutologinReq(msgEncoded);
                this.marsClient.$store.commit("authenticateUser", {username: message.username, password: ""});
                console.log(this.marsClient.$store.state.mars.credential);
                const wasAlreadyAuthorised = this.marsClient.$store.state.mars.credential.authorised === true;
                this.marsClient.$store.commit('authorizeUser');
                if( ! wasAlreadyAuthorised ){
                    // ... almost always it's a developer that was turning the server up/down frequently
                    //console.log('creating the database client side');
                    this.marsClient.$store.commit('setDataStatus', 'creatingDatabase');
                    this.marsClient.createDatabase(message.sqlCreateDatabase);
                    this.marsClient.prepareStatements(message.sqlStatements);
                    this.marsClient.prepareJsStatements(message.jsStatements);
                    // ... if we don't have cached tables, don't stick with loading
                    let cachedTables: boolean = false;
                    for( let cached of this.marsClient.isCachedTable){ 
                        cachedTables = cachedTables || cached;
                    }
                    if( cachedTables ){
                        this.marsClient.$store.commit('setDataStatus', 'waitingForLoading');
                    }
                    else {
                        this.marsClient.$store.commit('setDataStatus', 'waitingForOperations');
                    }
                } else skipImportValues = true;
            }
            else if( msgType == MsgTypeStoC.pingReq ){
                console.log('mars - ping keep alive request from server in S to C channel');
            }
            else if( msgType == MsgTypeStoC.syncOperationReq ){
                const message = decodeSyncOperationReq(msgEncoded);
                if( message.operation == SyncOperation.starting ){
                    this.marsClient.$store.commit('setDataStatus', 'loading');
                }
                else if( message.operation == SyncOperation.completed ){
                    this.marsClient.$store.commit('setDataStatus', 'waitingForOperations');
                }
                else console.log('ERROR, unknown sync operation in sync operation request');
                this.sendMessageReply(msgId, MsgTypeStoC.syncOperationRep, encodeSyncOperationRep());
            }
            else if( msgType == MsgTypeStoC.importRecordsReq ){
                if( skipImportValues ){
                    console.log('C <-- S | import values, but skipping');
                }
                else {
                    console.log('C <-- S | import values');
                    const message = decodeImportRecordsReq(msgEncoded);
                    this.marsClient.importRecords(message.tableIndex, message.statementIndex, message.encodedRecords);
                    console.log('C --> S | import values reply');
                    this.sendMessageReply(msgId, MsgTypeStoC.importRecordsRep, encodeImportRecordsRep());
                }
            }
            else if( msgType == MsgTypeStoC.insertRecordsReq ){
                console.log('S --> C |  id:%d insert records', msgId);
                const message = decodeInsertRecordsReq(msgEncoded);
                this.marsClient.insertRecords(message.tableIndex, message.statementIndex, message.encodedRecords);
                console.log('S <-- C | insert records reply');
                this.sendMessageReply(msgId, MsgType.insertValuesReply, encodeInsertRecordsRep());
            }
            else if( msgType == MsgTypeStoC.deleteRecordsReq ){
                console.log("C <-- S | delete records request");
                const message = decodeDeleteRecordsReq(msgEncoded);
                this.marsClient.deleteRecords(message.tableIndex, message.statementIndex, message.encodedRecords);
                console.log('C --> S | delete record reply');
                this.sendMessageReply(msgId, MsgTypeStoC.deleteRecordsRep, encodeDeleteRecordsRep());
            }
            else if( msgType == MsgTypeStoC.updateRecordsReq ){
                console.log("C <-- S | update records request");
                const message = decodeUpdateRecordsReq(msgEncoded);
                this.marsClient.updateRecords(message.tableIndex, message.encodedRecords);
                console.log("C --> S | update records reply");
                this.sendMessageReply(msgId, MsgTypeStoC.updateRecordsRep, encodeUpdateRecordsRep());
            }
            // XXX Hack for the 2.0 release of SZS
            else if( msgType == MsgType.updateValuesRequest ){
                console.log('C <-- S |  update values');
                const message = updateValuesRequest(msgEncoded);
                this.marsClient.updateValues(23, message.statementIndex, message.bytes);
                console.log('C --> S | update values reply');
                this.sendMessageReply(msgId, MsgType.updateValuesReply, updateValuesReply());
            }
            else {
                console.log('C <-- S | received an unexpected push message of type:%d!', msgType);
                // XXX disconnect!
            }
            
            this.marsClient.$store.commit('setConnectionStatus', 'waitingForMessages');
            //console.log('mars - socket on message terminated, ready to receive an other message');
        };
    }

    sendMessageRequest(type: any, msg: Object)
    {
        let prefix = new ArrayBuffer(8);
        let ui32 = new Uint32Array(prefix);
        ui32[0] = this.nextId; 
        ui32[1] = type;
        this.nextId += 1;

        this.messages.set(ui32[0], msg);
        const encoded = msgpack.encode(msg);
        
        const tmp = new Uint8Array(prefix.byteLength + encoded.byteLength);
        tmp.set(new Uint8Array(prefix), 0);
        tmp.set(new Uint8Array(encoded), prefix.byteLength);
        const buffer = tmp.buffer;

        //console.log('mars - send message request %d of type %d', ui32[0], type);
        this.marsClient.$store.commit('setConnectionStatus', 'handlingMessages');
        this.socket.send(buffer);
        this.marsClient.$store.commit('setConnectionStatus', 'waitingForMessages');
        //console.log('mars - sendMessageRequest exiting');
    }

    sendMessageReply(id: any, type: any, msg: any) 
    {
        let prefix = new ArrayBuffer(8);
        let ui32 = new Uint32Array(prefix);
        ui32[0] = id; 
        ui32[1] = type;

        const encoded = msgpack.encode(msg);
        
        const tmp = new Uint8Array(prefix.byteLength + encoded.byteLength);
        tmp.set(new Uint8Array(prefix), 0);
        tmp.set(new Uint8Array(encoded), prefix.byteLength);
        const buffer = tmp.buffer;

        console.log('mars - S<--C - id:%d send message reply of type %d', ui32[0], type);
        this.marsClient.$store.commit('setConnectionStatus', 'handlingMessages');
        this.socket.send(buffer);
        this.marsClient.$store.commit('setConnectionStatus', 'waitingForMessages');
    }
};
