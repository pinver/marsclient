

import * as alasql from "alasql";

//var alasql: any = null;
import { connectTo, connectFrom }  from './sockets';
import { MsgHandlerTo, MsgHandlerFrom, MsgType,
    frzInsertValuesRequest, frzDeleteRecordRequest, mltDeleteRecordRequest, frzCallServerMethodRequest,
    RequestState,
    OptUpdateReq, OptUpdateRep, encodeOptUpdateReq,
    PesUpdateReq, PesUpdateRep, encodePesUpdateReq,
    UpdateRecord,
    SubscribeReq, SubscribeRep, encodeSubscribeReq,
    encodePingReq,
} from './msg';

// keep it in sync with the function in server.d
interface IIndexStatementFor {
    (table: number, op : string): number;
};

var indexStatementFor: IIndexStatementFor;


export class MarsClient 
{
    url: string;
    identifier: number;
    db: any;
    msgId: number;
    messages: any;
    $store: any;
    clientid: string;
    state: string;
    socketTo: WebSocket; // socket to use for request to the service
    socketFrom: WebSocket; // socket with request coming from the service
    msgHandlerTo: MsgHandlerTo;
    msgHandlerFrom: MsgHandlerFrom;
    sqlStatements: Array<any>;
    onConnected: any;
    jsStatements: Array<any>;
    schema: Array<Object>;
    isMetaSyncTable: Array<boolean>; // is the table decorated with metadata about sync?
    isCachedTable: Array<boolean>; // is the table cached in the server and in the client?
    pingTimer: number; // the id of the time that's sending periodic keep alive to the server

    /**
     * The client is instantiated only one time, when the mars support is installed */
    initializeClient(url: any) {
        this.url = url;
        // ... the identifier is used by the server to understand if a client is reconnecting after that the
        //     connection was lost ... that behaviour is usual in development, when the server is restarted
        //     often during the edit-compile-run cycle ...
        this.identifier = Math.floor((Math.random() * 1000) + 1);
        
        this.db = new (alasql as any).Database('mars');

        this.msgId = 0; // used to track message request/response
        this.messages = new Map();
        this.onConnected = () => console.log('onConnected');
        this.pingTimer = 0;
    }

    initializeCommunication(store: any, onConnected: any) {
        this.onConnected = onConnected;
        this.$store = store;
        connectTo(this.url, this);
    }

    // a socket connection is now ready, handshake with the server 
    beginComunication() {
        console.log('mars - starting to handshake with the server');
        
        this.clientid = 'client_' + this.identifier.toString();

        this.state = 'helo';
        this.socketTo.send('mars');
    }

    onMessageTo(msg: any): void {
        //console.log('mars - received message from websocket:"%s"', msg.data);
        if( this.state === 'helo' ){
            if( msg.data === 'marsserver0000' ){
                console.log('C <-- S | helo');
                this.state = 'identifying';
                console.log('C --> S | client id %s', this.clientid);
                this.socketTo.send(this.clientid);
            }
            else {
                console.log('C <-- S | unexpected reply from mars server (data:%s), disconnecting', msg.data);
                this.socketTo.close();
                this.socketFrom.close();
                this.$store.commit('setConnectionStatus', 'waitingForReconnection');
            }
        }
        else if( this.state === 'identifying' ){
            if( msg.data === 'marswelcome' || msg.data === 'marsreconnected' ){
                console.log('C <-- S | marswelcome / marsreconnected');
                this.state = 'connected';
                // ... we are ready, from now on, the communication is handled by the message handler
                this.msgHandlerTo = new MsgHandlerTo(this.socketTo, this);
                connectFrom(this.url, this);
                this.msgHandlerFrom = new MsgHandlerFrom(this.socketFrom, this);
                // ... keep alive the connection via explicit message
                this.pingTimer = setInterval( () => this.msgHandlerTo.sendReq(null, encodePingReq()), 10000);
                // ... let's signal the app that we are connected to the server ...
                this.$store.commit('setConnectionStatus', 'waitingForMessages');
                // XXX ???? ma da dove viene? this.onConnected();
                const credential = this.$store.state.mars.credential;
                if( credential.authorised === true ){
                    console.log('trying to re-authorise the user %s', credential.username);
                    this.$store.dispatch('loginUser', credential.username, credential.password);
                }
            }

            else {
                console.log('mars - unexpected reply from mars server, disconnecting');
                if( this.pingTimer !== 0 ){
                    clearInterval(this.pingTimer);
                    this.pingTimer = 0;
                }
                this.socketTo.close();
                this.socketFrom.close();
                this.$store.commit('setConnectionStatus', 'waitingForReconnection');
            }
        }
    }

    createDatabase(sql: any) {
        //console.log('mars - creating database:%s', sql);
        this.db.exec(sql);
        //console.log('mars - database ready to be filled');
    }

    dropDatabase() {
        //console.log('mars - creating database:%s', sql);
        const tables = this.db.exec("SHOW TABLES");
        tables.forEach( (table: any) => this.db.exec("DROP TABLE " + table.tableid));
        //console.log('mars - database ready to be filled');
    }
    
    prepareStatements(sqlStatements: any) {
        console.log('mars - preparing sql statements');
        this.sqlStatements = new Array();
        for( let i =0; i <sqlStatements.length; ++i ){
            console.log('mars - statement:%d %s', i, sqlStatements[i]);
            let stat = (alasql as any).compile(sqlStatements[i], 'mars');
            this.sqlStatements.push(stat);
        }
        console.log('mars - prepared %d sql statements', this.sqlStatements.length);
    }

    prepareJsStatements(jsStatements: any) {
        console.log('mars - preparing javascript statements');
        indexStatementFor    = eval(jsStatements[0]);
        this.isMetaSyncTable = eval(jsStatements[1]);
        this.isCachedTable   = eval(jsStatements[2]);
        this.schema          = JSON.parse(jsStatements[3]);
        this.$store.commit('setSchema', JSON.parse(jsStatements[3]));
        this.jsStatements = new Array();
        for( let i =4; i < jsStatements.length; ++i ){
            console.log('mars - statement:%s', jsStatements[i]);
            let stat: any = eval(jsStatements[i]);
            this.jsStatements.push(stat);
        }
    }

    asPkParamStruct(table: number, key: Object): any      { return this.jsStatements[table *3 +0](key); }
    asPkParamWhereStruct(table: number, key: Object): any { return this.jsStatements[table *3 +1](key); }
    referencesFor(table: number): any                     { return this.jsStatements[table *3 +2](); }

    importRecords(tableIndex: number, statementIndex: number, records: Array<Object>): void {
        const sql: any = this.sqlStatements[statementIndex];
        for(let i =0; i <records.length; ++i ){
            const record = records[i];
            //console.log('and values are:', record);
            try {
                sql(record);
            } 
            catch(e) {
                // ... this is now needed as we have not-lazy table that are referring to not loaded 'lazy' table:
                //     foreign key errors.
            }
        }
        this.triggerUpdates(tableIndex);
        //console.log('mars - imported records into table');
    }

    deleteRecords(tableIndex: number, statementIndex: number, records: Array<Object>): void {
        console.log('mars - deleting values from table');
        const sql: any = this.sqlStatements[indexStatementFor(tableIndex, 'updateDecorations')];
        for(let i=0; i <records.length; ++i ){
            const record = records[i];
            //console.log("record:", record);
            sql(record);
        }
        this.triggerUpdates(tableIndex);
        setTimeout( () => {
            const sql: any = this.sqlStatements[indexStatementFor(tableIndex, 'delete')];
            for(let i = 0; i < records.length; ++i ){
                const record = records[i];
                sql(record);
            }
            this.triggerUpdates(tableIndex);
        }, 2000);
    }

    insertRecords(tableIndex: number, statementIndex: number, records: Array<Object>): void {
        if( ! this.isCachedTable[tableIndex] ) alert('unexpected insert in not cached table.');
        console.log('mars - inserting values into table');
        const sql: any = this.sqlStatements[statementIndex];
        for(let i =0; i <records.length; ++i ){
            const record = records[i];
            try {
                sql(record);
            }
            // XXX this is a problem! right now, we are receiving from server an insert also
            // for a client side insert performed by this client. must be handled server side.
            catch(e){
                console.log('tacheeeee!', e);
            }
        }
        //console.log('mars - inserted values into table');
        this.triggerUpdates(tableIndex);
    }

    updateRecords(tableIndex: number, records: Array<UpdateRecord>): void {
        if( ! this.isCachedTable[tableIndex] ) alert('unexpected update record from server for a not cached table.');
        const sql: any = this.sqlStatements[indexStatementFor(tableIndex, 'updateDecoratedRecord')];
        for(let i =0; i < records.length; ++i ){
            const record = records[i];
            const keys = this.asPkParamWhereStruct(tableIndex, record.keys);
            sql((<any>Object).assign({}, record.record, keys));

        }
        this.triggerUpdates(tableIndex);
    }
    
    // server side update of the value, here we have the notification
    updateValues(tableIndex: number, statementIndex: number, values: any) {
        //console.log('mars - updating values into table');
        const sql: any = this.sqlStatements[statementIndex];
        for(let i =0; i <values.length; ++i ){
            const vvv = values[i];
            //console.log('and values are:', vvv);
            let args = vvv.record;
            for(var key in vvv.keys) args["key" + key]=vvv.keys[key];
            //console.log('and arguments are:', args);
            sql(args);
        }
        this.triggerUpdates(tableIndex);
        //console.log('mars - updated values into table');
    }


    vueInsertRecord(table: number, record: any){
        const tableIndex = table;

        const clonedRecord = JSON.parse(JSON.stringify(record));

        // ... let's insert optimistically the record in alasql if the table is cached
        if( this.isCachedTable[tableIndex] ){
            record.mars_who = this.$store.state.mars.credential.username + '@' + this.clientid;
            record.mars_when = new Date().toString()
            record.mars_what = 'opt_insert';


            const sql: any = this.sqlStatements[indexStatementFor(tableIndex, "insert")];
            try {
                sql(record);
            }
            catch(e){
                if(e.message === 'Cannot insert record, because it already exists in primary key index'){
                    alert('Cannot insert record, because it already exists in primary key index');
                    return;
                }
            }
        }
    
        // ... let's send the request to the server
        this.msgHandlerTo.sendMessageRequest(MsgType.insertValuesRequest, frzInsertValuesRequest(tableIndex, clonedRecord));
        // ... if we are not cached, we need to wait the reply
        if( this.isCachedTable[tableIndex] ) this.triggerUpdates(tableIndex);
    }

    onVueInsertRecordReply(msgRequest: any, msgReply: any)
    {
        if( msgReply.statementIndex == -1 ){
            console.log("The server failed to unpack the record to insert!");
            alert("The server failed to unpack the record to insert.");
        }
        else {
            console.log("the reply I've received", msgReply);
            if( msgReply.insertStatus === 1 ){   // inserted
                if( this.isCachedTable[msgReply.tableIndex] ){
                    const sql: any = this.sqlStatements[msgReply.statementIndex];
                    const parameters = (<any>Object).assign({},  msgReply.bytes, msgReply.clientKeys);
                    ///** Deep clone obects */ // in alasql: Uint8Array, dobbiamo supportarlo....
                    sql(parameters);
                    // right now, update as a second step the decorations, can be merged with the above if we provide a sql
                    const sql2: any = this.sqlStatements[msgReply.statementIndex2];
                    sql2((<any>Object).assign({}, { mars_who: this.$store.state.mars.credential.username + '@' + this.clientid, mars_when: new Date().toString(), mars_what: 'inserted' }, msgReply.clientKeys));
                }
                this.triggerUpdates(msgReply.tableIndex);
            }
            else if( msgReply.insertStatus === 2 ){ // duplicateKeyViolations
                alert('violazione chiave duplicata, lato server');
                if( this.isCachedTable[msgReply.tableIndex] ){
                    // update the decorations, signal the duplicate key
                    const sql: any = this.sqlStatements[msgReply.statementIndex];
                    const parameters = (<any>Object).assign({}, { mars_who: this.$store.state.mars.credential.username + '@' + this.clientid, mars_when: new Date().toString(), mars_what:'duplicateKeyViolations'}, msgReply.clientKeys);
                    console.log('parameters:', parameters);
                    sql(parameters);
                    this.triggerUpdates(msgReply.tableIndex);
                    // delete the record after a while
                    setTimeout( () => {
                        console.log('deleting...', msgReply);
                        const sql: any = this.sqlStatements[msgReply.statementIndex2];
                        sql(msgReply.clientKeys);
                        this.triggerUpdates(msgReply.tableIndex);
                        console.log('deleted', this);
                    }, 2000);
                }
            }
            else if( msgReply.insertStatus === 3 ){ // unknown server errors
                alert('server side unknown error on insert'); // XXX curry record with proper information
                if( this.isCachedTable[msgReply.tableIndex] ){
                    const sql: any = this.sqlStatements[msgReply.statementIndex];
                    sql(msgReply.clientKeys);
                }
                this.triggerUpdates(msgReply.tableIndex);
            }
        }
    }

    vueDeleteRecord(tableIndex: number, record: Object): void {
        // see #3 of marservice: we are not able to reinsert the record right now on failure
        /*
        if( this.isCachedTable[tableIndex] ){
            const sql: any = this.sqlStatements[indexStatementFor(tableIndex, "delete")];
            let k = this.asPkParamWhereStruct(tableIndex, record);
            console.log('mars - deleting using sql statement ', indexStatementFor(tableIndex, "delete"), " with where ", k);
            sql(k); // XXX quando può fallire? vedi sopra, in vueInsertRecord
        }
        */

        // ... let's send the request to the server
        const keys = this.asPkParamWhereStruct(tableIndex, record);
        this.msgHandlerTo.sendMessageRequest(MsgType.deleteRecordRequest, frzDeleteRecordRequest(tableIndex, keys));
        // #3
        /*
        this.triggerUpdates(tableIndex);
        */
    }

    onVueDeleteRecordReply(msgRequest: any, msgReply: any){
        //console.log(msgRequest);
        if( msgReply.statementIndex == -1 ){
            console.log("The server failed to unpack the record to delete!");
            alert("The server failed to unpack the record to delete");
        }
        else {
            //console.log(msgReply);
            if( msgReply.deleteStatus == 1 ){ // deleted
                if( ! this.isCachedTable[msgRequest.statementIndex] ){
                    // it is NOT cached, we need to refresh totally
                    this.triggerUpdates(msgRequest.statementIndex);
                }
                // #3, see above - delete right now (not optimistically, so!)
                else if( this.isCachedTable[msgRequest.statementIndex] ){
                    const req = mltDeleteRecordRequest(msgRequest);
                    
                    const sql: any = this.sqlStatements[indexStatementFor(req.statementIndex, "delete")];
                    console.log('mars - deleting using sql statement ', indexStatementFor(req.statementIndex, "delete"), " with where ", req.decodedBytes);
                    sql(req.decodedBytes);

                    this.triggerUpdates(req.statementIndex);
                }
            }
            else if( msgReply.deleteStatus == 3 || // rejected as referenced
                    msgReply.deleteStatus == 2     // unknown server error
            ){
                if( msgReply.deleteStatus == 3 )
                    alert('non è possibile cancellare questa voce: è tuttora citata in altre tabelle.');
                else if(msgReply.deleteStatus == 2)
                    alert('server side unknown error on delete');

                // #3, we should reinsert it
                /*
                if( this.isCachedTable[msgRequest.tableIndex] ){
                    const sql: any = this.sqlStatements[msgReply.statementIndex];
                    sql(msgReply.serverRecord);
                }
                this.triggerUpdates(msgRequest.tableIndex);
                */
            }
        }
    }
    /**
     * Update a record in a pessimistic way, so wait for response.
     * 
     * @param tableIndex the numeric index of the table
     * @param keys the keys of the record to update
     * @param record the new values
     */
    updateRecordPes(tableIndex: number, keys: Object, record: Object): void {
        if( JSON.stringify(this.asPkParamStruct(tableIndex, keys)) !== JSON.stringify(keys)){
            alert("wrong keys in update! (wrong names, or some key is missing)");
            console.error(JSON.stringify(this.asPkParamStruct(tableIndex, keys)));
            console.error(JSON.stringify(keys));
            return;
        }
        // ... switch from { a: 1 } to { keya: 1 }
        const whereKeys = this.asPkParamWhereStruct(tableIndex, keys);
        // ... let's send the request to the server
        const req: PesUpdateReq = { tableIndex, keys, record };
        this.msgHandlerTo.sendReq(req, encodePesUpdateReq(req));
    }

    onPesUpdateRep(req: PesUpdateReq, rep: PesUpdateRep): void 
    {
        if( rep.state === RequestState.rejectedAsWrongParameter ||
            rep.state === RequestState.rejectedAsDecodingFailed
        ){
            const msg: string = "bug: pessimistic update failed:" + rep.state;
            console.error(msg); alert(msg);
        }
        else if( rep.state === RequestState.rejectedAsForeignKeyViolation ){
            const msg: string = "Modifica non possibile, poichè un riferimento a questo valore di dati è ancora presente nel sistema";
            console.error(msg); alert(msg);
        }
        // ... success, the server has updated the table ...
        else if( rep.state === RequestState.executed ){
            // ... if the table is not cached, re-execute the query.
            //     server-side, we are not able to send just the diff right now, and in any case, a re-execution of the
            //     query is unavoidable
            this.triggerUpdates(req.tableIndex);
        }
        else {
            const msg: string = "bug: unexpected state";
            console.error(msg); alert(msg);
        }
    }


    vueUpdateRecord(tableIndex: number, keys: Object, record: Object): void {
        if( JSON.stringify(this.asPkParamStruct(tableIndex, keys)) !== JSON.stringify(keys)){
            alert("wrong keys in update! (wrong names, or some key is missing)");
            console.error(JSON.stringify(this.asPkParamStruct(tableIndex, keys)));
            console.error(JSON.stringify(keys));
            return;
        }
        // ... switch from { a: 1 } to { keya: 1 }
        const whereKeys = this.asPkParamWhereStruct(tableIndex, keys);
        // ... as we eventually need to access the Sync metadata when processing the reply
        let sync = {};

        // ... optimistically update the table, but saving the original object, if it's cached
        let sql: any = this.sqlStatements[indexStatementFor(tableIndex, "selectFromWhere")];
        let originalRecords: Array<Object> = sql(whereKeys);
        if( originalRecords.length !=1 ){
            const msg: string = 
              "Bug: Optimistic update of table "+ tableIndex+
              " select for original row returned none or multiple results. (" + originalRecords.length + ")";
            console.log(msg); console.log(whereKeys, originalRecords);
            alert(msg);
        }
        let originalRecord: Object = originalRecords[0];
        if( this.isCachedTable[tableIndex] ){
            if(this.isMetaSyncTable[tableIndex]){
                sql = this.sqlStatements[indexStatementFor(tableIndex, "update")];
                sql((<any>Object).assign({}, record, whereKeys));
                sql = this.sqlStatements[indexStatementFor(tableIndex, "updateDecorations")];
                sync = {
                    mars_who: this.$store.state.mars.credential.username + '@' + this.clientid,
                    mars_when: new Date().toString(),
                    mars_what:'optupdated'
                };
                sql((<any>Object).assign({}, sync, whereKeys));
            }
            else {
                const sql: any = this.sqlStatements[indexStatementFor(tableIndex, "update")];
                sql((<any>Object).assign({}, record, whereKeys));
            }
        }

        // ... let's send the request to the server
        const req: OptUpdateReq = { tableIndex, keys, record, sync, originalRecord };
        this.msgHandlerTo.sendReq(req, encodeOptUpdateReq(req));
        this.triggerUpdates(tableIndex);
    }

    private revertOptimisticUpdate(req: OptUpdateReq): void 
    {
        // ... if the table is cached, patch it locally
        if( this.isCachedTable[req.tableIndex] ){
            if(this.isMetaSyncTable[req.tableIndex]){
                const updatedKeys = this.asPkParamWhereStruct(req.tableIndex, req.record);
                let sql: any = this.sqlStatements[indexStatementFor(req.tableIndex, "update")];
                sql((<any>Object).assign({}, req.originalRecord, updatedKeys));
            }
            else {
                // extract the keys of the optimistically updated record
                const whereKeys: Object = this.asPkParamWhereStruct(req.tableIndex, req.record);
                const sql: any = this.sqlStatements[indexStatementFor(req.tableIndex, "update")];
                sql((<any>Object).assign({}, req.originalRecord, whereKeys));
            }
            this.triggerUpdates(req.tableIndex);
        }
    }
    onVueUpdateReply(req: OptUpdateReq, rep: OptUpdateRep): void 
    {
        if( rep.state === RequestState.rejectedAsWrongParameter ||
            rep.state === RequestState.rejectedAsDecodingFailed
        ){
            const msg: string = "bug: Optimistic update failed:" + rep.state + "reverting"; 
            console.error(msg);
            this.revertOptimisticUpdate(req);
        }
        else if( rep.state === RequestState.rejectedAsForeignKeyViolation ){
            const msg: string = "Modifica non possibile, poichè un riferimento a questo valore di dati è ancora presente nel sistema";
            console.error(msg); alert(msg);
            this.revertOptimisticUpdate(req);
        }
        // ... success, the server has updated the table ...
        else if( rep.state === RequestState.executed ){
            if( this.isCachedTable ){
                // extrack the keys of the optimistically updated record
                const whereKeys: Object = this.asPkParamWhereStruct(req.tableIndex, req.record);
                // XXX should I update also all the other fields? yep! Where is the server updated record? Check!
                if(this.isMetaSyncTable[req.tableIndex]){
                    const sql: any = this.sqlStatements[indexStatementFor(req.tableIndex, "updateDecorations")];
                    sql((<any>Object).assign({}, { mars_what: "updated", mars_when: req.sync.mars_when, mars_who: req.sync.mars_who }, whereKeys));
                }
                this.triggerUpdates(req.tableIndex);
            }
            // ... if the table is not cached, re-execute the query.
            //     server-side, we are not able to send just the diff right now, and in any case, a re-execution of the
            //     query is unavoidable
            else {
                this.triggerUpdates(req.tableIndex);
            }
        }
        else {
            const msg: string = "bug: unexpected state";
            console.error(msg); console.error(msg);
        }
    }

    // if the table was subscribed, trigger an update
    triggerUpdates(tableIndex: number){
        let subscription = this.subscriptions.get(tableIndex);
        if( subscription ){
            // ... some VM is subscribed to this table
            for( let [vm, toUpdates] of subscription ){
                // ... this vm is subscribed, more that one query in the VM to update
                for( let toUpdate of toUpdates ){
                    toUpdate(this.$store.state.mars.schema);
                }
            }
        }
    }

    /**
     * Subscribe a Vue VM using a table name to receive table data.
     * 
     * @param vmName - 
     * @param tableName - the name of the table in the sql
     * @param updateDatas: -
     */
    subscribeToTable(vmName: number, tableName: string, updateDatas: Array<UpdateData> ): void
    {
        if( theMarsClient.$store.state.mars.schema[tableName] === undefined ) 
            alert('bug: table <' + tableName + '> does not exists');
        let tableIndex = theMarsClient.$store.state.mars.schema[tableName].index;
        this.subscribe(vmName, tableIndex, updateDatas);
    }
    
    // deprecated the public usage, use the above function, that accepts a table name instead of an index.
    subscribe(vmName: number, table: number, updateDatas: Array<UpdateData> ): void 
    {
        // extract the subscriptions for that table
        let a = this.subscriptions.get(table);
        if( a == null ){
            a = new Map<number, Array<UpdateData> >();
            this.subscriptions.set(table, a);
        }

        // sanity check
        if( a.has(vmName) ) alert("this vm is already subscribed to this table?, replacing...");

        // set the new subscription
        a.set(vmName, updateDatas);
    }

    unsubscribe(vmName: number, table: number): void
    {
        let subscription = this.subscriptions.get(table);
        if( subscription ){
            if( subscription.has(vmName) ){
                subscription.delete(vmName);
            }
            else alert("unsubscribe of not subscribed vm?");
            
        }
        else {
            console.log("vmName:", vmName, " table:", table, "subscriptions:", this.subscriptions);
            alert("unsubscribe of not subscribed table?");
        }
    }

    subscriptions: Map<number, Map<number, Array<UpdateData> > > = new Map<number, Map<number, Array<UpdateData> > >();
}
export interface UpdateData {
        (schema: Object): void;
}

export
var theMarsClient = new MarsClient();


class DollarMars 
{
    vm: any;
    
    dataName: string;
    select: string; 
    table: number;
    parameters: Function;

    constructor(vm: any) { this.vm = vm; }
    client() { return theMarsClient; }

    // the web client is requesting an insert.
    // Params:
    //     table = the table numeric identifier
    //     record = Json object with _all_ the columns and _all_ the values to insert
    insertRecord(table: number | string, record: Object){
        let tableIndex: number = 0;
        if( typeof table === "string" ) tableIndex = theMarsClient.$store.state.mars.schema[table].index;
        else tableIndex = table;
        if( tableIndex === undefined ) alert('wrong table name/number in insertRecord:' + table);

        theMarsClient.vueInsertRecord(tableIndex, record);
    }


    // the web client is requesting a delete.
    // Params:
    //     table = the table numeric identifier
    //     record = Json object with _all_ the columns and _all_ the values to delete.
    deleteRecord(table: number | string, record: Object){
        let tableIndex: number = 0;
        if( typeof table === "string" ) tableIndex = theMarsClient.$store.state.mars.schema[table].index;
        else tableIndex = table;
        if( tableIndex === undefined ) alert('wrong table name/number in deleteRecord:' + table);

        theMarsClient.vueDeleteRecord(tableIndex, record);
    }

    // the web client is requesting an update of a record.
    // Params:
    //     table = the table numeric identifier
    //     keys = Json object with the primary keys values of the record to update.
    //     record = Json object with _all_ the new values of the record.
    updateRecord(table: number | string, keys: Object, record: Object){
        let tableIndex: number = 0;
        if( typeof table === "string" ) tableIndex = theMarsClient.$store.state.mars.schema[table].index;
        else tableIndex = table;
        if( table === undefined ) alert('wrong table name/number in updateRecord:' + table);

        if( theMarsClient.isCachedTable[tableIndex] ){
            theMarsClient.vueUpdateRecord(tableIndex, keys, record);
        }
        else {
            theMarsClient.updateRecordPes(tableIndex, keys, record);
        }
    }

    /**
     * Handle the mars options defined in the vue component, during the creation ...
     * 
     * @param name - the name of the data in the vm (ex. people)
     * @param options - json with the options for that data (ex, { select: 'select * ..', table: 2, .. })
     */
    option(name: any, options: any) {
        //console.log('mars.DollarMars.option(name:%s, options:%s)', name, options);

        // safety checks
        if( ! ("table" in options) ){ alert('missing table number in mars declaration! '+ JSON.stringify(options)); }
        for(let property in options.parameters) {
            if(options.parameters[property] === null) {
                alert('Parameter value is "null" '+ JSON.stringify(options));
            }
        }

        this.dataName = name;
        this.select = options.select;
        this.table = options.table;
        this.parameters = options.parameters;

        // ... when there's the need to refresh the data, this delegate is called
        const updateTheData = (schema: any) => {
            let tableIndex: number = -1;
            if( typeof (options.table) === "string" ){
                if( schema[options.table] === undefined ) alert('table <' + options.table + '> does not exists');
                tableIndex = schema[options.table].index;
            }
            else {
                tableIndex = options.table;
            }

            if( tableIndex >= theMarsClient.isCachedTable.length ) alert('wrong table number in query: '+ tableIndex);

            let records = [];
            if(! theMarsClient.isCachedTable[tableIndex]){ // XXX and for complex query?
                if( ! options.parameters ){
                    const req: SubscribeReq = { select: options.select, parameters: "", vm: this.vm, name, columns: options.columns };
                    theMarsClient.msgHandlerTo.sendReq(req, encodeSubscribeReq(req));
                }
                else {
                    const req: SubscribeReq = { select: options.select, parameters: JSON.stringify(options.parameters.bind(this.vm)()), vm: this.vm, name, columns: options.columns };
                    theMarsClient.msgHandlerTo.sendReq(req, encodeSubscribeReq(req));
                }
                this.vm[name + '_mars_status'] = { loading: true };
            }
            else {
                if( ! options.parameters ){ // ,, use 'options' and not 'this', this is a lambda
                records = theMarsClient.db.exec(options.select);
                }
                else {
                    let compiled = (alasql as any).compile(options.select, 'mars');
                    records = compiled(options.parameters.bind(this.vm)());
                }
            }
            this.vm[name] = records;
            this.vm[name + "_mars_definition"] = { references: theMarsClient.referencesFor(tableIndex) };
        }

        // ... refresh the data if the parameters of the query were modified
        if( options.parameters ){
            this.vm.$watch(options.parameters, updateTheData, { deep: true });
        }

        // ... register the updater delegate, so that Mars can update the data when the server push some operation
        let subscription = this.subscriptions.get(options.table);
        if( subscription ){} else {
            subscription = new Array<UpdateData>();
            this.subscriptions.set(options.table, subscription);
        }
        subscription.push( (schema) => updateTheData(schema) );
    }

    // once we have collected all the options declared in the VM, now subscribe via the marsClientInstance
    registerSubscriptions(){
        if( this.subscriptions.size == 0 ) console.log('mars - warn, mars component but no subscription to data');
 
        // ... check if we can update right now ...
        if( theMarsClient.$store.state.mars.dataStatus == 'waitingForOperations' ){

            let toBeUpdated: UpdateData[] = [];
            for( let [table_, updates] of this.subscriptions )
            {
                let tableIndex: number = -1;
                if( typeof (table_) === "string" ){
                    const table: string = table_;
                    if( theMarsClient.$store.state.mars.schema[table] === undefined ) alert('table <' + table + '> does not exists');
                    tableIndex = theMarsClient.$store.state.mars.schema[table].index;
                }
                else {
                    tableIndex = table_;
                }
                this.client().subscribe(this.vm._uid, tableIndex, updates);
                toBeUpdated = toBeUpdated.concat(updates);
            }
            for( let update of toBeUpdated ){ update(this.vm.$store.state.mars.schema); }
        }
        // ... if not, check again after one second, for ten times
        else {
            let howManyTimes = 0;
            const handle = setInterval( () => {
                console.log('%d, check if the db is idle... ', howManyTimes, theMarsClient.$store.state.mars.dataStatus);
                if( theMarsClient.$store.state.mars.dataStatus == 'waitingForOperations' ){

                    let toBeUpdated: UpdateData[] = [];
                    for( let [table_, updates] of this.subscriptions ){
                        let tableIndex: number = -1;
                        if( typeof (table_) === "string" ){
                            const table: string = table_;
                            if( theMarsClient.$store.state.mars.schema[table] === undefined ) alert('table <' + table_ + '> does not exists');
                            tableIndex = theMarsClient.$store.state.mars.schema[table].index;
                        }
                        else {
                            tableIndex = theMarsClient.$store.state.mars.table;
                        }
                        this.client().subscribe(this.vm._uid, tableIndex, updates);
                        toBeUpdated = toBeUpdated.concat(updates);
                    }
                    for( let update of toBeUpdated ){ update(this.vm.$store.state.mars.schema); }
                    clearInterval(handle);
                }
                else {
                    howManyTimes ++;
                    if( howManyTimes >= 30 ){
                        clearInterval(handle);
                        console.log('the database is not ready for operations, I give up!');
                    }
                }
            }, 1000);
        }
    }

    subscriptions: Map<number, Array<UpdateData> > = new Map<number, Array<UpdateData> >();

}

const mixinDollarMars = function(this: any)
{
    //console.log('mars.mixinDollarMars(this:%s)', this);
    Object.defineProperty(this as any, '$mars', {
        get: () => {
            if( ! this._mars ){ this._mars = new DollarMars(this); }
            return this._mars;
        }
    });
};

const scanComponentForMars = function(this: any) {
    //console.log('mars.scanComponentForMars(this:%s)', this);

    // this is the 'mars:' attribute in the Vue components...
    let mars = this.$options.mars;

    if( mars ){
        //console.log('mars.scanComponentForMars(this:%s) - the component is using Mars.', this);
        for( let key in mars ){
            this.$mars.option(key, mars[key]); // process the mars definition in the component
        }
        this.$mars.registerSubscriptions();
    }
};

const removeSubscriptions = function(this: any) {
    for( let table of this.$mars.subscriptions.keys() ){
        // ... in dollar mars, subscriptions can be numbers (old style) or strings (new style)
        //     while in mars client they are numbers (as mapped in the 'schema')
        let tableIndex: number = -1;
        if( typeof (table) === "string" ){
            if( (theMarsClient.schema as any)[table] === undefined ) alert('table <' + table + '> does not exists');
            tableIndex = (theMarsClient.schema as any)[table].index;
        }
        else {
            tableIndex = table;
        }
        theMarsClient.unsubscribe(this._uid, tableIndex);
   } 
}

export
interface Credential {
    username: String;
    password: String;
}

export
interface ServerMethod {
    method: string;
    parameters: Object;
    callback: Function;
}

export 
let marsStore = 
{
    state: {
        connectionStatus: 'connecting', // waitingForReconnection, waitingForMessages, handlingMessages

        // current status of the database, client side:
        //    waitingForDefinitions: initial status, the schema and the definitions are not available
        //    creatingDatabase: the server has sent the schema, and we are creating the database
        //    waitingForLoading: the database, the schema, and injected JS is ready, but there's no data.
        //    loading: data is flowing from the server to the client, filling the caches; that's
        //             toggled by the SyncOperations, 
        //    waitingForOperations: the data is ready to be used by the components
        dataStatus: 'waitingForDefinitions',

        credential: { username: '', password: '', authorised: false, errors: '' },
        serverMethod: { method: '', parameters: '', returns: '', callback: null },

        schema: {},
    },
    getters: {
        authorised: function(state: any) { return state.credential.authorised; },
    },
    mutations: {
        setConnectionStatus(localState: any, connectionStatus: any) {
            localState.connectionStatus = connectionStatus;
            //console.log('mars - store connection status set to %s', connectionStatus);
        },
        setDataStatus(localState: any, dataStatus: any) {
            localState.dataStatus = dataStatus;
        },

        setSchema(localState: any, schema: any){ localState.schema = schema; },

        authenticateUser(localState: any, credential: Credential){
            if( credential.username != localState.credential.username || credential.password != localState.credential.password ){
                localState.credential = { username: credential.username, password: credential.password, authorised: false };
            }
            else console.log('mars - same credential for current user, that is authenticated:%s', localState.credential.authorised);
            localState.credential.errors = '';
        },

        authorizeUser(localState: any){
            localState.credential = { 
                username: localState.credential.username, 
                password: localState.credential.password, 
                authorised: true, errors: '' 
            };
        },

        deauthorizeUser(localState: any, { errors }: { errors: string } ){
            localState.credential = { 
                username: localState.credential.username, 
                password: localState.credential.password, 
                authorised: false, errors: errors 
            };
        },

        setCurrentServerMethod(localState: any, serverMethod: ServerMethod) {
            localState.serverMethod = serverMethod;
        },

        callServerMethodReply(localState: any, args: any[]){
            localState.serverMethod.method = args[0];
            localState.serverMethod.parameters = args[1];
            localState.serverMethod.returns = args[2];
            if(localState.serverMethod.callback)
                localState.serverMethod.callback();
        }
    },
    actions: {
        loginUser(context: any, credential: Credential){
            context.commit('authenticateUser', credential);
            console.log('C --> S | authenticate username:%s', credential.username);
            theMarsClient.msgHandlerTo.sendMessageRequest(0, { username: credential.username });
        },
        logoutUser(context: any){
            context.commit('deauthorizeUser', { errors: "" });
            theMarsClient.msgHandlerTo.sendMessageRequest(4, {}); 
        },
        callServerMethod(context: any, serverMethod: ServerMethod ){
            console.log('C --> S | call server method:%s parameters:%s', serverMethod.method, serverMethod.parameters, serverMethod.callback );
            context.commit('setCurrentServerMethod', serverMethod);
            theMarsClient.msgHandlerTo.sendMessageRequest(150, frzCallServerMethodRequest(serverMethod.method, serverMethod.parameters));
        }
    }
};

var xxx = { 'mixinDollarMars': mixinDollarMars, 'scanComponentForMars': scanComponentForMars, 'removeSubscriptions': removeSubscriptions, 'MarsClient': MarsClient };

export function marsInstall(Vue: any, options: any): void
{
    // ... I wasn't able to require directly old versions of alasql in this module, now it's obsoleted 
    if( options.alasql ) 
        console.log('MARSCLIENT DEPRECATION: passing alasql module as an option is deprecated, just drop it in the code.');
console.log("url is:>", options.url, ">");
    // ... this will last all the session, and it's not re-instantiated if the connection with the mars server is
    //     dropped or lost ...
    theMarsClient.initializeClient(options.url);

    Vue.mixin({
        beforeCreate: xxx.mixinDollarMars, // ... inject mars into every vue components ...
        created: xxx.scanComponentForMars, // ... verify if the component is actually using mars ...
        beforeDestroy: xxx.removeSubscriptions, // ... remove subscriptions for refresh of sql data
    });
};

